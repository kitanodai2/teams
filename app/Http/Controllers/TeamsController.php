<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;

class TeamsController extends Controller
{
    //
    public function index()
    {
        $teams = Team::all();
        $max = Team::max('number');
        $rest = Team::where('name',null)->count();
        return view('index',compact('teams','max','rest'));
    }
    //
    public function update(Request $request)
    {
        $name = $request->input('user_name');
        $target = Team::where('name',null)->inRandomOrder()->first();
        $target->name = $name;
        $target->save();
        $number = $target->number;
        $teams = Team::all();
        $max = Team::max('number');
        //二重送信防止
        $request->session()->regenerateToken();
        return view('result',compact('name','max','teams','number'));
    }
    public function config()
    {
        return view('config.index');
    }
    //
    public function show($cnt)
    {
        return view('config.show',compact('cnt'));
    }
    //
    public function create(Request $request)
    {
        $action = $request->get('action');
        $post_data = $request->except(['_token','action']);
        // 確認画面で戻るボタンが押された場合
        if ($request->get('action') === 'back') {
          // 入力画面へ戻る
          return view('config.back',compact('post_data'));
        // 確認画面で作成ボタンが押された場合
        }else{
          // チームを作成する
          Team::truncate();
          foreach($post_data as $data => $value){
            $number = str_replace('team','',$data);
            for($i=1;$i<=$value;$i++){
              $team = new Team();
              $team->create([
                'number' => $number,
              ]);
            }
          }
          // ホームへリダイレクト
          return redirect()->route('home');
        }
    }
}
