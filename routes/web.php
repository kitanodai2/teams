<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Http\Request;
use App\Team;

Route::get('/', 'TeamsController@index')->name('home');

Route::post('/update', 'TeamsController@update');

Route::get('/config', function(){
    $max = 9;
    return view('config.index',compact('max'));
});

Route::post('/config', function(Request $request){
    $cnt = $request->input('count');
    return view('config.form',compact('cnt'));
});

Route::post('/config/confirm', function(Request $request){
    $post_data = $request->except(['_token']);
    return view('config.confirm',compact('post_data'));
});

Route::post('/config/create', 'TeamsController@create');



