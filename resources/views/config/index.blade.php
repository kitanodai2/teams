@extends('layout')
@section('link')
          <a href="{{url('/')}}" class="navbar-brand d-flex align-items-center">
@endsection
@section('icon')
            <i class="fas fa-home mr-2"></i>
@endsection
@section('content')
      <div class="container text-center">
        <h3 class="my-5">チーム設定</h3>
        <p class="lead text-muted mb-4">チーム数を選択</p>
        <form action="{{url('/config/')}}" method="post">
          @csrf
@for($i=1;$i<$max;$i++)
@if($i%4==1)
          <p>
@endif
            <input type="submit" name="count" value="{{$i+1}}" class="btn btn-primary mr-2" style="width:50px;">
@endfor
@if($i%4==1)
          </p>
@endif
        </form>
      </div>
@endsection
