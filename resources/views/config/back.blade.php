@extends('layout')
@section('link')
          <a href="{{url('/')}}" class="navbar-brand d-flex align-items-center">
@endsection
@section('icon')
            <i class="fas fa-home mr-2"></i>
@endsection
@section('content')
      <div class="container text-center">
        <h3 class="my-5">チーム設定</h3>
        <p class="lead text-muted mb-4">各チームの人数を入力</p>
        <form action="{{url('/config/confirm')}}" method="post">
          @csrf
          @foreach($post_data as $data => $value)
          <div class="form-group row w-50 mx-auto">
            <label class="col col-form-label text-nowrap">{{$data}}</label>
            <div class="col">
              <input type="number" name="{{$data}}" value="{{$value}}" class="form-control text-center" max=30 min=1 required>
            </div>
          </div>
          @endforeach
          <a href="{{url('/config/')}}" class="btn btn-large btn-outline-primary mt-3 mb-5">戻る</a>
          <button type="submit" class="btn btn-large btn-primary mt-3 mb-5">決定</button>
        </form>
      </div>

@endsection
