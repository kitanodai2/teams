<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    //タイムスタンプを使用しない
    public $timestamps = false;
    //name, numberカラムにデータの挿入を許可する
    protected $fillable = [
        'name',
        'number',
    ];    
}
