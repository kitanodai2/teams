@extends('layout')
@section('link')
          <a href="{{url('/')}}" class="navbar-brand d-flex align-items-center">
@endsection
@section('icon')
            <i class="fas fa-home mr-2"></i>
@endsection
@section('content')
      <section class="jumbotron text-center">
        <div class="container">
          <p class="result-number mx-auto mb-3">{{$number}}</h1>
          <p class="lead text-muted">{{$name}}さんはチーム{{$number}}です。</p>
        </div>
      </section>
      <div class="bg-white">
        <div class="container">
          <div class="card-deck text-center">
          @for($i=1;$i<=$max;$i++)
              <div class="card mb-4 shadow">
                <div class="card-header bg-primary text-light"><h4>{{$i}}</h4></div>
                <div class="card-body">
          @foreach($teams as $team)
          @if($team->number==$i)
                  @empty($team->name)
                  <p class="text-light w-75 mx-auto p-2" style="background-color:#BDC3C7;">?</p>
                  @else
                  <p class="w-75 mx-auto p-2" style="background-color:yellow;">{{$team->name}}</p>
                  @endempty
          @endif
          @endforeach
                </div>
              </div>
          @endfor
          </div>
        </div>
      </div>
@endsection
