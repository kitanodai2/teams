@extends('layout')
@section('link')
          <a href="{{url('/')}}" class="navbar-brand d-flex align-items-center">
@endsection
@section('icon')
            <i class="fas fa-home mr-2"></i>
@endsection
@section('content')
      <div class="container text-center">
        <h3 class="my-5">チーム設定の確認</h3>
        <form action="{{url('/config/create')}}" method="post">
          @csrf
          @foreach($post_data as $data => $value)
            <p class="mb-4">{{$data}}<span class="badge badge-pill ml-2 p-2" style="background-color:yellow;">{{$value}}人</span></p>
            <input type="hidden" name="{{$data}}" value="{{$value}}">
          @endforeach
          <p class="lead text-muted mb-4">この内容でチームを作成しますか？</p>
          <button type="submit" name="action" value="back" class="btn btn-large btn-outline-primary mb-5">戻る</button>
          <button type="submit" name="action"  value="post" class="btn btn-large btn-primary mb-5">作成</button>
        </form>
      </div>
@endsection
