$(function(){
  $('#back_to_top').hide();
  
  $(window).scroll(function() {
    if($(this).scrollTop() > 500) {
      $('#back_to_top').fadeIn("slow");
    } else {
      $('#back_to_top').fadeOut("slow");
    }
  });

  $('#back_to_top').click(function(){
    $('html, body').animate({
      'scrollTop':0
    },500);
    return false;
  });

});