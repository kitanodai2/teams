@extends('layout')
@section('link')
          <a href="{{url('/config/')}}" class="navbar-brand d-flex align-items-center">
@endsection
@section('icon')
            <i class="fas fa-cog mr-2"></i>
@endsection
@section('content')
      <section class="jumbotron text-center">
        <div class="container">
          <h1 class="jumbotron-heading mb-3 display-4">Teams</h1>
          <p class="lead text-muted mb-4">チーム分けアプリ</p>
          @if($rest > 0)
          <p><a href="#" class="btn-circle-flat shadow" data-toggle="modal" data-target="#modal">くじを引く</a></p>
          <p>残り{{$rest}}個</p>
          @else
          <p class="btn-circle-flat-end">終了</p>
          @endif
        </div>
        <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="label1" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="label1">くじを引く</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
              <form action="{{url('/update')}}" method="post">
              @csrf
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label">名前を入力</label>
                  <div class="col-sm-9">
                    <input type="text" name="user_name" class="form-control" required>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">閉じる</button>
                <button type="submit" name="post" class="btn btn-primary">決定</button>
              </div>
              </form>
            </div>
          </div>
        </div>
      </section>
      <div class="bg-white">
        <div class="container">
          <div class="card-deck text-center">
          @for($i=1;$i<=$max;$i++)
              <div class="card mb-4 shadow">
                <div class="card-header bg-primary text-light"><h4>{{$i}}</h4></div>
                <div class="card-body">
          @foreach($teams as $team)
          @if($team->number==$i)
                  @empty($team->name)
                  <p class="text-light w-75 mx-auto p-2" style="background-color:#BDC3C7;">?</p>
                  @else
                  <p class="w-75 mx-auto p-2" style="background-color:yellow;">{{$team->name}}</p>
                  @endempty
          @endif
          @endforeach
                </div>
              </div>
          @endfor
          </div>
        </div>
      </div>
@endsection
